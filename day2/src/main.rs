use std::default;
use std::ops::AddAssign;

fn main() {
    let day1_demo = include_str!("../../input/day2_demo.txt");
    let day1 = include_str!("../../input/day2.txt");


    let mut sum  = 0;
    let mut power = 0;

    for line in day1.lines() {
        let game = Game::from_line(line);

        if game.check_rule(12, 13, 14) {
            sum.add_assign(game.id)
        }

        power.add_assign(game.power());
    }


    println!("Task 1: {sum}");
    println!("Task 2: {power}");
}

#[derive(Debug)]
struct Round {
    red: usize,
    blue: usize,
    green: usize,
}

#[derive(Debug)]
struct Game {
    id: usize,
    rounds: Vec<Round>,
    reds_found: usize,
    blues_found: usize,
    greens_found: usize,
}

impl Game {
    pub fn from_line(line: &str) -> Self {
        let split = line.split_once(":").unwrap();

        let id = split.0.split_at(4).1.trim().parse().unwrap();

        let mut rounds = vec![];

        let mut reds_found = 0;
        let mut blues_found = 0;
        let mut greens_found = 0;

        for play in split.1.split(";") {
            let mut red = 0;
            let mut blue = 0;
            let mut green = 0;
            for card in play.split(",") {
                let parts = card.trim().split_once(" ").unwrap();

                let color = parts.1.trim();
                let number: usize = parts.0.trim().parse().unwrap();

                match color {
                    "red" => { red.add_assign(number); if number > reds_found { reds_found = number } }
                    "blue" => { blue.add_assign(number); if number > blues_found { blues_found = number } }
                    "green" => { green.add_assign(number); if number > greens_found {greens_found = number } }
                    _ => {}
                }
            }

            rounds.push(Round {
                red,
                blue,
                green,
            })
        }

        Self {
            id,
            rounds,
            reds_found,
            blues_found,
            greens_found,
        }
    }

    pub(crate) fn check_rule(&self, red: usize, green: usize, blue: usize) -> bool {
        for round in &self.rounds {
            if round.red > red || round.green > green || round.blue > blue {
                dbg!(self);
                return false
            }
        }

        true
    }

    pub(crate) fn power(&self) -> usize {
        self.blues_found * self.reds_found * self.greens_found
    }
}
