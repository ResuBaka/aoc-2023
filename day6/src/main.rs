use std::fmt::format;
use std::ops::Add;
use std::time::Instant;

fn main() {
    let demo = include_str!("../../input/day6_demo.txt");
    let input = include_str!("../../input/day6.txt");


    let used_input = input;

    let now = Instant::now();

    let mut sum = 0;
    let values = used_input.lines().map(|x| {
        x.split_whitespace().filter(|x| !x.trim().is_empty()).filter(|x| !x.starts_with("T")).filter(|x| !x.starts_with("D")).map(|x| x.parse().unwrap()).collect::<Vec<usize>>()
    }).collect::<Vec<Vec<usize>>>();

    for index in 0..values[0].len() {
        let time = values[0][index];
        let distance = values[1][index];

        let mut max_speed = 0;
        let mut min_speed = 0;

        for t in 1..=time {
            let speed = t;
            let time_to_drive = time - t;
            if (speed * time_to_drive) > distance {
                max_speed = speed;
                break;
            }
        }

        for t in (1..=time).rev() {
            let speed = t;
            let time_to_drive = time - t;
            if (speed * time_to_drive) > distance {
                min_speed = speed;
                break;
            }
        }

        println!("Min {min_speed}, Max {max_speed} Windows {}", (max_speed..min_speed).len() + 1);

        if index > 0 {
            sum *= (max_speed..min_speed).len() + 1;
        } else {
            sum = (max_speed..min_speed).len() + 1
        }
    }

    println!("Time: {:?}", Instant::now() - now);


    println!("Task 1: {sum}");


    let now = Instant::now();

    let mut sum = 0;
    let values = used_input.lines().map(|x| {
        x
            .split_whitespace()
            .filter(|x| !x.trim().is_empty())
            .filter(|x| !x.starts_with("T"))
            .filter(|x| !x.starts_with("D"))
            .fold("".to_string(),|acc, e| acc.add(e))
            .parse().unwrap()
    }).collect::<Vec<usize>>();

    let time = values[0];
    let distance = values[1];

    let mut max_speed = 0;
    let mut min_speed = 0;

    for t in 1..=time {
        let speed = t;
        let time_to_drive = time - t;
        if (speed * time_to_drive) > distance {
            max_speed = speed;
            break;
        }
    }

    for t in (1..=time).rev() {
        let speed = t;
        let time_to_drive = time - t;
        if (speed * time_to_drive) > distance {
            min_speed = speed;
            break;
        }
    }

    println!("Min {min_speed}, Max {max_speed} Windows {}", (max_speed..min_speed).len() + 1);

    sum = (max_speed..min_speed).len() + 1;

    println!("Time: {:?}", Instant::now() - now);


    println!("Task 2: {sum}")
}
