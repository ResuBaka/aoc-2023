use std::collections::{HashMap, HashSet};
use std::ops::AddAssign;
use std::time::Instant;

fn main() {
    let _demo = include_str!("../../input/day4_demo.txt");
    let input = include_str!("../../input/day4.txt");

    // let input = demo;

    let mut sum = 0;
    let mut sum2 = 0;

    let mut game_wins = vec![0; input.lines().count()];

    let mut game_wins_hashmap = HashMap::with_capacity(input.lines().count());

    for index in 0..input.lines().count() {
        game_wins_hashmap.insert(index, 1);
    }

    println!("Games: {}", game_wins.len());

    for line in input.lines() {
        let numbers = line.split_once(":").unwrap().1.split_whitespace();

        let mut winnings = true;
        let mut set = HashSet::new();
        let mut temp_sum = 0;

        for number in numbers {
            let number = number.trim();
            match number {
                "|" => winnings = false,
                _ => {
                    if winnings {
                        set.insert(number);
                    } else {
                        if set.contains(number) {
                            if temp_sum == 0 {
                                temp_sum = 1;
                            } else {
                                temp_sum *= 2
                            }
                        }
                    }
                }
            }
        }

        sum.add_assign(temp_sum);
    }

    let now = Instant::now();
    for (index, line) in input.lines().enumerate() {
        // println!("{line}");
        let numbers = line.split_once(":").unwrap().1.split_whitespace();

        // for play in 0..=game_wins[index] {
        let mut winnings = true;
        let mut set = HashSet::new();
        // println!("Play {play}");
        let mut wins_round = 0;
        let games_played = game_wins_hashmap.get(&index).unwrap().clone();
        println!("Game {index} {games_played}");
        for number in numbers.clone() {
            let number = number.trim();
            match number {
                "|" => winnings = false,
                _ => {
                    if winnings {
                        set.insert(number);
                    } else {
                        if set.contains(number) {
                            wins_round += 1;
                            game_wins_hashmap
                                .get_mut(&(index + wins_round))
                                .unwrap()
                                .add_assign(games_played);
                        }
                    }
                }
            }
            // }

            // println!("I have wins = {wins_round}");

            for index_win in 1..=wins_round {
                // println!("add wins {} > {}", game_wins.len() , index + index_win);
                if game_wins.len() > index + index_win {
                    game_wins[index + index_win] += 1;
                }
            }
        }

        // println!("{game_wins:?}")
    }

    for game_win in game_wins_hashmap {
        sum2 += game_win.1;
    }

    println!("Time: {:?}", Instant::now() - now);

    println!("Task 1: {sum}");
    println!("Task 1: {sum2}")
}
