use std::fmt::format;
use std::ops::AddAssign;
use std::time::Instant;

fn main() {
    let day1 = include_str!("../../input/day1.txt");

    let number_as_name = vec![
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ];

    println!("Smart Char Logic ->");
    let now = Instant::now();
    better_solution_v2(day1, &number_as_name);
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
    println!();

    println!("Smart Parse Logic ->");
    let now = Instant::now();
    better_solution(day1, &number_as_name);
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
    println!();

    println!("Not Smart Char Logic ->");
    let now = Instant::now();
    orignial_solution_v2(day1, &number_as_name);
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
    println!();

    println!("Not Smart Parse Logic ->");
    let now = Instant::now();
    orignial_solution(day1, &number_as_name);
    let elapsed = now.elapsed();
    println!("Elapsed: {:.2?}", elapsed);
}

fn better_solution_v2(day1: &str, number_as_name: &Vec<(&str, usize)>) {
    let mut sum_1 = 0;
    let mut sum_2 = 0;

    for line in day1.lines() {
        let mut first = 0;
        let mut second = 0;

        for char in line.chars() {
            let num = (char as u32 - 48) as usize;
            if num <= 9 {
                first = num;
                break
            }
        }

        for char in line.chars().rev() {
            let num = (char as u32 - 48) as usize;
            if num <= 9 {
                second = num;
                break
            }
        }

        sum_1.add_assign(
            (first * 10) + second
        )
    }

    println!("New 1. {sum_1}");

    for line in day1.lines() {
        let mut first = 0;
        let mut second = 0;

        'out: for index in 0..line.len() {
            let search_string = line.split_at(index).1;
            for nan in number_as_name {
                if search_string.starts_with(nan.0) {
                    first = nan.1;
                    break 'out;
                }
            }

            let char_to_check = search_string.chars().next().unwrap();
            let num = (char_to_check as u32 - 48) as usize;
            if num <= 9 {
                first = num;
                break 'out;
            }
        }

        'out: for index in (0..line.len()).rev() {
            let search_string = line.split_at(index).1;

            for nan in number_as_name {
                if search_string.starts_with(nan.0) {
                    second = nan.1;
                    break 'out;
                }
            }

            let char_to_check = search_string.chars().next().unwrap();
            let num = (char_to_check as u32 - 48) as usize;
            if num <= 9 {
                second = num;
                break 'out;
            }
        }

        sum_2.add_assign(
            (first * 10) + second
        )
    }

    println!("New 2. {sum_2}");
}
fn better_solution(day1: &str, number_as_name: &Vec<(&str, usize)>) {
    let mut sum_1 = 0;
    let mut sum_2 = 0;

    for line in day1.lines() {
        let mut first = None;
        let mut second = None;

        for char in line.chars() {
            if let Ok(number) = char.to_string().parse::<usize>() {
                first = Some(number);
                break
            }
        }

        for char in line.chars().rev() {
            if let Ok(number) = char.to_string().parse::<usize>() {
                second = Some(number);
                break
            }
        }

        sum_1.add_assign(format!("{}{}", first.unwrap(), second.unwrap()).parse::<usize>().unwrap())
    }

    println!("New 1. {sum_1}");

    for line in day1.lines() {
        let mut first = None;
        let mut second = None;

        'out: for index in 0..line.len() {
            let search_string = line.split_at(index).1;
            for nan in number_as_name {
                if search_string.starts_with(nan.0) {
                    first = Some(nan.1);
                    break 'out;
                }
            }

            if let Ok(number) = search_string.split_at(1).0.parse::<usize>() {
                first = Some(number);
                break 'out;
            }
        }

        'out: for index in (0..line.len()).rev() {
            let search_string = line.split_at(index).1;

            for nan in number_as_name {
                if search_string.starts_with(nan.0) {
                    second = Some(nan.1);
                    break 'out;
                }
            }

            if let Ok(number) = search_string.split_at(1).0.parse::<usize>() {
                second = Some(number);
                break 'out;
            }
        }

        sum_2.add_assign(format!("{}{}", first.unwrap(), second.unwrap()).parse::<usize>().unwrap())
    }

    println!("New 2. {sum_2}");
}

fn orignial_solution_v2(day1: &str, numbers_as_name: &Vec<(&str, usize)>) {
    let mut values = vec![];
    let mut values2 = vec![];



    for line in day1.lines() {
        let mut tmp = vec![];
        for char in line.chars() {
            let num = (char as u32 - 48) as usize;
            if num <= 9 {
                tmp.push(num);
            }
        }

        values.push(tmp)
    }

    for line in day1.lines() {
        let mut tmp = vec![];

        name_v2(line, &mut tmp, &numbers_as_name);

        values2.push(tmp)
    }

    let mut sum = 0;
    let mut sum2 = 0;

    for value in values {
        sum.add_assign(
            (value.first().unwrap() * 10) + value.last().unwrap()
        )
    }

    for value2 in values2 {
        sum2.add_assign(
        (value2.first().unwrap() * 10) + value2.last().unwrap()
        )
    }

    println!("Sum is {sum}");
    println!("Sum2 is {sum2}")
}
fn orignial_solution(day1: &str, numbers_as_name: &Vec<(&str, usize)>) {
    let mut values = vec![];
    let mut values2 = vec![];



    for line in day1.lines() {
        let mut tmp = vec![];
        for char in line.chars() {
            if let Ok(number) = char.to_string().parse::<usize>() {
                tmp.push(number)
            }
        }


        values.push(tmp)
    }

    for line in day1.lines() {
        let mut tmp = vec![];

        name(line, &mut tmp, &numbers_as_name);

        values2.push(tmp)
    }

    let mut sum = 0;
    let mut sum2 = 0;

    for value in values {
        sum.add_assign(format!("{}{}", value.first().unwrap(), value.last().unwrap()).parse::<usize>().unwrap())
    }

    for value2 in values2 {
        sum2.add_assign(format!("{}{}", value2.first().unwrap(), value2.last().unwrap()).parse::<usize>().unwrap())
    }

    println!("Sum is {sum}");
    println!("Sum2 is {sum2}")
}


fn name(line: &str, numbers: &mut Vec<usize>, numbers_as_name: &Vec<(&str, usize)>) {
    let mut check = true;
    for nan in numbers_as_name {
        if line.starts_with(nan.0) {
            numbers.push(nan.1);

            if line.len() > nan.0.len() {
                let new_string = line.split_at(1).1;
                name(new_string, numbers, numbers_as_name);
            }
            check = false
        }
    }

    if check {
        let mut chars = line.chars();

        if let Some(char) = chars.next() {
            if let Ok(number) = char.to_string().parse::<usize>() {
                numbers.push(number);
                if line.len() > 1 {
                    name(line.split_at(1).1, numbers, numbers_as_name);
                }
            } else {
                name(chars.as_str(), numbers, numbers_as_name);
            }
        }
    }
}

fn name_v2(line: &str, numbers: &mut Vec<usize>, numbers_as_name: &Vec<(&str, usize)>) {
    let mut check = true;
    for nan in numbers_as_name {
        if line.starts_with(nan.0) {
            numbers.push(nan.1);

            if line.len() > nan.0.len() {
                let new_string = line.split_at(1).1;
                name_v2(new_string, numbers, numbers_as_name);
            }
            check = false
        }
    }

    if check {
        let mut chars = line.chars();

        if let Some(char) = chars.next() {
            let num = (char as u32 - 48) as usize;
            if num <= 9 {
                numbers.push(num);
                if line.len() > 1 {
                    name_v2(line.split_at(1).1, numbers, numbers_as_name);
                }
            } else {
                name_v2(chars.as_str(), numbers, numbers_as_name);
            }
        }
    }
}
