use std::time::Instant;
use rayon::prelude::*;

fn main() {
    let demo = include_str!("../../input/day5_demo.txt");
    let input = include_str!("../../input/day5.txt");


    let used_input = input;

    let mut lookups = vec![];

    let mut lines = used_input.lines();

    let now = Instant::now();

    let (_, right) = lines.next().unwrap().split_once(":").unwrap();
    let seeds = right.split_whitespace().map(|num| num.trim().parse().unwrap()).collect::<Vec<usize>>();

    let mut parse_lookups = vec![];

    let mut small = usize::MAX;
    let mut small2 = usize::MAX;

    let _ = lines.next();

    for line in lines {
        if line.is_empty() {
            lookups.push(parse_lookups.clone());
            parse_lookups.clear();
            continue;
        }

        if line.contains("map") {
            continue;
        }

        let numbers = line.split_whitespace().map(|num| num.trim().parse().unwrap()).collect::<Vec<usize>>();

        parse_lookups.push(Lookup {
            destination: numbers[0],
            source: numbers[1],
            range: numbers[2],
        })
    }

    lookups.push(parse_lookups.clone());
    parse_lookups.clear();

    println!("Lookpus {}", lookups.len());

    for seed in seeds.clone() {
        let mut check = seed;
        for lookup in &lookups {
            for l in lookup {
                let result = l.position(check);

                if check != result {
                    check = result;
                    break;
                }
            }
        }

        if check < small {
            small = check
        }
    }
    {
        println!("Time: {:?}", Instant::now() - now);

        println!("Task 1: {small}");
        let now = Instant::now();

        let mut seeds_day2 = vec![];

        for chunk in seeds.chunks(2) {
            let a = chunk.get(0).unwrap();
            for ch in 0..*chunk.get(1).unwrap() {
                seeds_day2.push(a + ch)
            }
        }

        let target = seeds_day2.par_iter().map(|seed| {
            let mut check = seed.clone();
            for lookup in &lookups {
                for l in lookup {
                    let result = l.position(check);

                    if check != result {
                        check = result;
                        break;
                    }
                }
            }

            check
        }).min().unwrap();

        println!("Time: {:?}", Instant::now() - now);

        println!("Task 2: {target}");
    }

    let now = Instant::now();

    let mut seeds_day2 = vec![];

    for chunk in seeds.chunks(2) {
        let a = chunk.get(0).unwrap();
        for ch in 0..*chunk.get(1).unwrap() {
            seeds_day2.push(a + ch)
        }
    }


    for seed in seeds_day2 {
        let mut check = seed;
        for lookup in &lookups {
            for l in lookup {
                let result = l.position(check);

                if check != result {
                    check = result;
                    break;
                }
            }
        }

        if check < small2 {
            small2 = check
        }
    }
    println!("Time: {:?}", Instant::now() - now);

    println!("Task 2: {small2}")
}

#[derive(Clone)]
struct Lookup {
    destination: usize,
    source: usize,
    range: usize,
}

impl Lookup {
    fn position(&self, pos: usize) -> usize {
        if pos < self.source {
            return pos;
        }

        if pos > self.source + (self.range - 1) {
            return pos;
        }

        let go = pos - self.source;


        self.destination + go
    }
}

#[cfg(test)]
mod tests {
    use crate::Lookup;

    #[test]
    fn it_works() {
        let l = Lookup {
            destination: 52,
            source: 50,
            range: 48,
        };
        assert_eq!(l.position(79), 81);
    }

    #[test]
    fn it_works2() {
        let l = Lookup {
            destination: 50,
            source: 98,
            range: 2,
        };
        assert_eq!(l.position(79), 79);
    }
}