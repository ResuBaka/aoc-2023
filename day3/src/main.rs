use std::any::Any;
use std::collections::{HashMap, HashSet};
use std::default;
use std::fmt::format;
use std::ops::AddAssign;

fn main() {
    let day3_demo = include_str!("../../input/day3_demo.txt");
    let day3 = include_str!("../../input/day3.txt");

    let input = day3;


    let height = input.lines().count();
    let with = input.lines().next().unwrap().len();

    let mut map = Vec::with_capacity(height);
    let mut map_symbol = HashMap::new();

    let mut sum  = 0;
    let mut power = 0;

    for (index, line) in input.lines().enumerate() {
        let mut line_vec = vec![];

        let mut peek = line.chars().peekable();

        while let Some(char) = peek.next() {
            // print!("{char} {}", char as u32);
            let start_pos = line_vec.len();
            match (char as u32) {
                number if number >= 48 && number <= 57 => {
                    let mut temp = char.to_string();


                    while let Some(a) = peek.next_if(|&x| x as u32 >= 48 && x as u32 <= 57) {
                        temp.add_assign(&a.to_string())
                    }

                    let number = temp.parse().unwrap();

                    for _ in 0..temp.len() {
                        line_vec.push(Type::Number(number, temp.len(), index, start_pos))
                    }
                }
                46 => { line_vec.push(Type::Placeholder) }
                _ => {
                    line_vec.push(Type::Symbol(char));
                    map_symbol.insert((index, start_pos), Type::Symbol(char));
                }
            }
        }

        map.push(line_vec);
    }

    for (index, line) in map.iter().enumerate() {

        let mut iter = line.iter();

        while let Some(line_element) = iter.next() {
            match line_element {
                Type::Number(number, length, i, start_pos) => {
                    let positions = get_post_to_check(length, i, start_pos, &height, &with);

                    for position in positions {
                        if map_symbol.contains_key(&position) {
                            sum += *number;
                            break;
                        }
                    }

                    for _ in 0..*length {
                        iter.next();
                    }
                }
                Type::Placeholder => {}
                Type::Symbol(_) => {}
            }
        }
    }

    map_symbol.iter().filter(|(key, value)| {
        match value {
            Type::Symbol(char) if *char == '*' => {
                true
            }
            _ => false
        }
    }).for_each(|(char, sum)| {
        let a = get_post_to_check(&1, &char.0, &char.1, &height, &with);

        let mut temp = 0;
        let mut first_number = None;

        for (index, position) in a.iter().enumerate() {
            if let Some(number) = map.get(position.0).unwrap().get(position.1) {
                match number {
                    Type::Number(inner_number, _, _, _) => {

                        if first_number.is_none() {
                            first_number = Some(number);
                        }

                        if Some(number) != first_number {
                            match first_number {
                                Some(Type::Number(f_number, _, _, _)) => {
                                    temp = f_number * inner_number;
                                }
                                _ => {}
                            }

                            break;
                        }
                    }
                    _ => {}
                }
            }
        }


        power += temp;

    });

    println!("Task 1: {sum}");
    println!("Task 2: {power}");
}

fn get_post_to_check(length: &usize, line: &usize, position: &usize, max_height: &usize, max_length: &usize) -> Vec<(usize, usize)> {
    let mut checks = vec![];

    let check_left = *position != 0;
    let check_right = *position + length != *max_length;
    let check_top = *line != 0;
    let check_bottom = line != max_height;

    if check_left {
        checks.push((*line, position - 1));
        if check_top { checks.push((line - 1, position - 1)) }
        if check_bottom { checks.push((line + 1, position - 1)) }
    }

    if check_top {
        for i in 0..*length {
            checks.push((line - 1, position + i));
        }
    }

    if check_bottom {
        for i in 0..*length {
            checks.push((line + 1, position + i));
        }
    }

    if check_right {
        checks.push((*line, position + length));
        if check_top { checks.push((line - 1, position + length)) }
        if check_bottom { checks.push((line + 1, position + length)) }
    }

    checks
}

#[derive(Debug, Eq, PartialEq)]
enum Type {
    Number(usize, usize, usize, usize),
    Placeholder,
    Symbol(char),
}